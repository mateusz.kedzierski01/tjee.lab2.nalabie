package Lab2.Dane;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class DbManager {
    public static final String DRIVER = "org.apache.derby.jdbc.ClientDriver";
    public static final String JDBC_URL = "jdbc:derby://localhost:1527/ludzie";
    
    private static java.sql.Connection conn;

    public static void insertData(String imie, String nazwisko, int id) throws SQLException {
        String UPDATE = "insert into PRACOWNICY (IMIE, NAZWISKO, ID) "
                + "VALUES('" + imie + "',  '" + nazwisko + "', " + id + ")";
        Statement stat = conn.createStatement();
        stat.executeUpdate(UPDATE);
        stat.close();
    }
    
    private DbManager() {
        
    }
    
    public static boolean Connect() {
        try {
            conn = DriverManager.getConnection(JDBC_URL);
        } catch (SQLException ex) {
            Logger.getLogger(DbManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }
    
    
    public static boolean Disconnect() {
        if (conn == null) {
            return false;
        } else {
            try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(DbManager.class.getName()).log(Level.SEVERE, null, ex);
            }
            return true;
        } 
    }
    
    public static String getData(String search, boolean col1, boolean col2, boolean col3) throws SQLException {
        String QUERY = "select * from app.pracownicy "
                + "where IMIE LIKE '%" + search + "%'"
                + " or NAZWISKO LIKE '%" + search + "%'";
        Statement stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(QUERY);
        ResultSetMetaData rsmd = rs.getMetaData();
        String wiersz = new String();
        
        int colCount = rsmd.getColumnCount();
        wiersz = wiersz.concat("<table><tr>");
        for (int i = 1; i <= colCount; i++) {
            if(i==1)
                if(col1==true)
                    wiersz = wiersz.concat(" <td><b> " + rsmd.getColumnName(i) + "</b></td> ");
            if(i==2)
                if(col2==true)
                    wiersz = wiersz.concat(" <td><b> " + rsmd.getColumnName(i) + "</b></td> ");
            if(i==3)
                if(col3==true)
                    wiersz = wiersz.concat(" <td><b> " + rsmd.getColumnName(i) + "</b></td> ");
        }
        wiersz = wiersz.concat("</tr>");    
        while (rs.next()) {
            wiersz = wiersz.concat("<tr>");
            for (int i = 1; i <= colCount; i++) {
                if(i==1)
                    if(col1==true)
                        wiersz = wiersz.concat(" <td> " + rs.getString(i) + " </td> ");
                if(i==2)
                    if(col2==true)
                        wiersz = wiersz.concat(" <td> " + rs.getString(i) + " </td> ");
                if(i==3)
                    if(col3==true)
                        wiersz = wiersz.concat(" <td> " + rs.getString(i) + " </td> ");
            }

            wiersz = wiersz.concat("</tr>");
        }
        wiersz = wiersz.concat("</table>");
 
 
        stat.close();
        return wiersz;
    } 
    
    public static String getData(boolean col1, boolean col2, boolean col3) throws SQLException {
        String QUERY = "select * from app.pracownicy";
        Statement stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(QUERY);
        ResultSetMetaData rsmd = rs.getMetaData();
        String wiersz = new String();
        
        int colCount = rsmd.getColumnCount();
        wiersz = wiersz.concat("<table><tr>");
        for (int i = 1; i <= colCount; i++) {
            if(i==1)
                if(col1==true)
                    wiersz = wiersz.concat(" <td><b> " + rsmd.getColumnName(i) + "</b></td> ");
            if(i==2)
                if(col2==true)
                    wiersz = wiersz.concat(" <td><b> " + rsmd.getColumnName(i) + "</b></td> ");
            if(i==3)
                if(col3==true)
                    wiersz = wiersz.concat(" <td><b> " + rsmd.getColumnName(i) + "</b></td> ");
        }
        wiersz = wiersz.concat("</tr>");    
        while (rs.next()) {
            wiersz = wiersz.concat("<tr>");
            for (int i = 1; i <= colCount; i++) {
                if(i==1)
                    if(col1==true)
                        wiersz = wiersz.concat(" <td> " + rs.getString(i) + " </td> ");
                if(i==2)
                    if(col2==true)
                        wiersz = wiersz.concat(" <td> " + rs.getString(i) + " </td> ");
                if(i==3)
                    if(col3==true)
                        wiersz = wiersz.concat(" <td> " + rs.getString(i) + " </td> ");
            }

            wiersz = wiersz.concat("</tr>");
        }
        wiersz = wiersz.concat("</table>");
 
 
        stat.close();
        return wiersz;
    } 
}