/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab2.Serwlety;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Mefju
 */
public class Wypisywanie extends HttpServlet {

    private boolean polaczenie = false;
    private String data;
    
    private String getDataFromDb(boolean col1, boolean col2, boolean col3) {
        
        polaczenie = Lab2.Dane.DbManager.Connect();
        if (polaczenie) {
            try {
                data = Lab2.Dane.DbManager.getData(col1, col2, col3);
            } catch (SQLException ex) {
                Logger.getLogger(Wypisywanie.class.getName()).log(Level.SEVERE, null, ex);
            }
            polaczenie = Lab2.Dane.DbManager.Disconnect();
        }
        
        return data;
    }  
    
    private String getDataFromDb(String search, boolean col1, boolean col2, boolean col3) {
        
        polaczenie = Lab2.Dane.DbManager.Connect();
        if (polaczenie) {
            try {
                data = Lab2.Dane.DbManager.getData(search, col1, col2, col3);
            } catch (SQLException ex) {
                Logger.getLogger(Wypisywanie.class.getName()).log(Level.SEVERE, null, ex);
            }
            polaczenie = Lab2.Dane.DbManager.Disconnect();
        }
        
        return data;
    }
 
 
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        response.setContentType("text/html; charset=UTF-8");
        String inst=request.getParameter("instytucja");
        String search=request.getParameter("search");
        
        String cb1=request.getParameter("cb1");
        String cb2=request.getParameter("cb2");
        String cb3=request.getParameter("cb3");
        
        boolean col1=(cb1!=null && cb1.equals("on"));
        boolean col2=(cb2!=null && cb2.equals("on"));
        boolean col3=(cb3!=null && cb3.equals("on"));
        
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head><meta><link rel='stylesheet' href='Style/css/components.css'>");
            out.println("<link rel='stylesheet' href='Style/css/icons.css'>");
            out.println("<link rel='stylesheet' href='Style/css/responsee.css'>");
            out.println("<body><h1>" + "Lista pracowników instytucji " + inst + "</h1><br />");
            if(search.isEmpty()) out.println(getDataFromDb(col1, col2, col3));
            else out.println(getDataFromDb(search, col1, col2, col3));
            out.println("<a class=\'button rounded-full-btn reload-btn s-2 margin-bottom\' href=");
            out.println(request.getHeader("referer"));
            out.println("><i class='icon-sli-arrow-left'>Powrót</i></a>");
            out.println("</body>");
            out.println("</html>");
        } 
    }


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    

}
