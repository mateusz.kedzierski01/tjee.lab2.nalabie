/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab2.Serwlety;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Mefju
 */
public class Dodawanie extends HttpServlet {

    private boolean polaczenie = false;
    private String data;
    
    private void addToDatabase(String imie, String nazwisko, int id) {
        polaczenie = Lab2.Dane.DbManager.Connect();
        if (polaczenie) {
            try {
                Lab2.Dane.DbManager.insertData(imie, nazwisko, id);
            } catch (SQLException ex) {
                Logger.getLogger(Dodawanie.class.getName()).log(Level.SEVERE, null, ex);
            }
            polaczenie = Lab2.Dane.DbManager.Disconnect();
        }
    }
 
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        response.setContentType("text/html; charset=UTF-8");
        String imie=request.getParameter("imie");
        String nazwisko=request.getParameter("nazwisko");
        int id=0;
        try{
            id=Integer.parseInt(request.getParameter("id"));
        } catch (NumberFormatException e){
            Logger.getLogger(Dodawanie.class.getName()).log(Level.SEVERE, null, e);
        }
        if(!imie.isEmpty() && !nazwisko.isEmpty() && id!=0){
            addToDatabase(imie, nazwisko, id);
            try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<meta charset=\"UTF-8\">");
            out.println("<link rel='stylesheet' href='Style/css/components.css'>");
            out.println("<link rel='stylesheet' href='Style/css/icons.css'>");
            out.println("<link rel='stylesheet' href='Style/css/responsee.css'>");
            out.println("</head>");
            out.println("<body><h1>" + "Dodano nowego pracownika (" + imie + " " + nazwisko + ") do bazy</h1><br />");
            out.println("<a class=\'button rounded-full-btn reload-btn s-2 margin-bottom\' href=");
            out.println(request.getHeader("referer"));
            out.println("><i class='icon-sli-arrow-left'>Powrót</i></a>");
            out.println("</body>");
            out.println("</html>");
            } 
        }else{
            try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<link rel='stylesheet' href='Style/css/components.css'>");
            out.println("<link rel='stylesheet' href='Style/css/icons.css'>");
            out.println("<link rel='stylesheet' href='Style/css/responsee.css'>");
            out.println("<meta charset=\"UTF-8\">");
            out.println("</head>");
            out.println("<body><h1>" + "Błędnie wprowadzono dane!</h1><br />");
            out.println("<a class=\'button rounded-full-btn reload-btn s-2 margin-bottom\' href=");
            out.println(request.getHeader("referer"));
            out.println("><i class='icon-sli-arrow-left'>Powrót</i></a>");
            out.println("</body>");
            out.println("</html>");
            } 
        }
    }


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    

}
